package devices;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.eseo.armange_thomas.eseoapp.R;


import java.util.List;

import data.People;

/**
 * Created by ARMANGE-Thomas on 17/01/2018.
 */

public class PeopleAdapter extends ArrayAdapter<People>{
    /**
     * Declare an inner interface to listen click event on device items
     */
    public interface OnDeviceSelectedListener {
        void handle(final People people);
    }

    private final OnDeviceSelectedListener onDeviceSelectedListener;

    public PeopleAdapter(@NonNull final Context context, final ArrayList<People> people, final OnDeviceSelectedListener listener) {
        super(context, R.layout.list_people, people);
        onDeviceSelectedListener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
        View holder = convertView;
        if (convertView == null) {
            final LayoutInflater vi = LayoutInflater.from(getContext());
            holder = vi.inflate(R.layout.list_people, null);
        }

        final People people= getItem(position);
        if (people == null) {
            return holder;
        }

        // display the name
        final TextView peopleName = holder.findViewById(R.id.textPeople);
        if (peopleName != null) {
            peopleName.setText(people.name);
        }

        // When this device item is clicked, trigger the listener
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (onDeviceSelectedListener != null) {
                    onDeviceSelectedListener.handle(people);
                }
            }
        });

        return holder;
    }
}
