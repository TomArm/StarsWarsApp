package com.eseo.armange_thomas.eseoapp.main.main;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.eseo.armange_thomas.eseoapp.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sw.StarWars;
import sw.StarWarsApi;
import data.*;
public class MainActivity extends AppCompatActivity {
    private ImageView logoImage;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logoImage = findViewById(R.id.logoImage);
        logoImage.setOnClickListener(onLogoImageClicked);

         class YourStarWarsApp extends Application {

            @Override
            public void onCreate() {
                super.onCreate();
                //Init star wars api
                StarWarsApi.init();
            }

        }


    }

    private final View.OnClickListener onLogoImageClicked = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            startActivity(MenuActivity.getStartIntent(MainActivity.this));
        }
    };

}

