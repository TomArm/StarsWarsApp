package devices;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eseo.armange_thomas.eseoapp.R;

import java.util.ArrayList;

import data.Films;
import data.Planets;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eseo.armange_thomas.eseoapp.R;

import java.util.ArrayList;

import data.People;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.eseo.armange_thomas.eseoapp.R;


import java.util.List;

import data.People;
import data.Planets;



/**
 * Created by ARMANGE-Thomas on 17/01/2018.
 */

public class FilmAdapter extends ArrayAdapter<Films> {
    /**
     *
     * Declare an inner interface to listen click event on device items
     */

    public interface OnDeviceSelectedListener {
        void handle(final Films films);
    }

    private final OnDeviceSelectedListener onDeviceSelectedListener;

    public FilmAdapter(@NonNull final Context context, final ArrayList<Films> films, final OnDeviceSelectedListener listener) {
        super(context, R.layout.list_film, films);
        onDeviceSelectedListener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
        View holder = convertView;
        if (convertView == null) {
            final LayoutInflater vi = LayoutInflater.from(getContext());
            holder = vi.inflate(R.layout.list_film, null);
        }

        final Films films= getItem(position);
        if (films == null) {
            return holder;
        }

        // display the name
        final TextView filmsName = holder.findViewById(R.id.textFilm);
        if (filmsName != null) {
            filmsName.setText(films.title);
        }

        // When this device item is clicked, trigger the listener
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (onDeviceSelectedListener != null) {
                    onDeviceSelectedListener.handle(films);
                }
            }
        });

        return holder;
    }
}
