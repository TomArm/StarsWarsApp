package devices;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eseo.armange_thomas.eseoapp.R;

import java.util.ArrayList;

import data.People;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.eseo.armange_thomas.eseoapp.R;


import java.util.List;

import data.People;
import data.Planets;

/**
 * Created by ARMANGE-Thomas on 17/01/2018.
 */

/**
 * Created by ARMANGE-Thomas on 17/01/2018.
 */

public class PlanetsAdapter extends ArrayAdapter<Planets> {
    /**
     * Declare an inner interface to listen click event on device items
     */
    public interface OnDeviceSelectedListener {
        void handle(final Planets planets);
    }

    private final OnDeviceSelectedListener onDeviceSelectedListener;

    public PlanetsAdapter(@NonNull final Context context, final ArrayList<Planets> planets, final OnDeviceSelectedListener listener) {
        super(context, R.layout.list_planets, planets);
        onDeviceSelectedListener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
        View holder = convertView;
        if (convertView == null) {
            final LayoutInflater vi = LayoutInflater.from(getContext());
            holder = vi.inflate(R.layout.list_planets, null);
        }

        final Planets planets= getItem(position);
        if (planets == null) {
            return holder;
        }

        // display the name
        final TextView planetsName = holder.findViewById(R.id.textPlanets);
        if (planetsName != null) {
            planetsName.setText(planets.name);
        }

        // When this device item is clicked, trigger the listener
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (onDeviceSelectedListener != null) {
                    onDeviceSelectedListener.handle(planets);
                }
            }
        });

        return holder;
    }
}
