package com.eseo.armange_thomas.eseoapp.main.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.eseo.armange_thomas.eseoapp.R;

import java.util.ArrayList;

import data.Films;
import data.People;
import data.Planets;
import data.SWModelList;
import devices.FilmAdapter;
import devices.PlanetsAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sw.StarWars;
import sw.StarWarsApi;

/**
 * Created by ARMANGE-Thomas on 16/01/2018.
 */

public class FilmActivity extends AppCompatActivity {

    FilmAdapter filmsAdapter;
    public static Intent getStartIntent(final Context ctx) {
        return new Intent(ctx, FilmActivity.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);
        final ArrayList<Films> filmArrayList=new ArrayList<Films>();
        //final ArrayAdapter<Films> adapter=new ArrayAdapter<Films>(this,R.layout.list_film,R.id.textFilm,filmArrayList);
        final ArrayList<String> filmName=new ArrayList<String>();
        final ArrayList<String> filmContenu=new ArrayList<String>();
        //final ArrayAdapter<String> adapter1=new ArrayAdapter<String>(this, R.layout.list_film, R.id.textFilm,filmName);
        filmsAdapter = new FilmAdapter(this,filmArrayList,filmSelectedListener);

        ListView listv=(ListView)findViewById(R.id.listFilm);
        listv.setAdapter(filmsAdapter);
        //listv.setAdapter(adapter1);
        //listv.setOnItemClickListener(new FilmActivity.ItemList());

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();
        api.getAllFilms(1, new Callback<SWModelList<Films>>() {
            @Override
            public void success(SWModelList<Films> filmsSWModelList, Response response) {
                filmArrayList.addAll(filmsSWModelList.results);
                filmsAdapter.notifyDataSetChanged();
            }
            @Override
            public void failure(RetrofitError error) {
                System.out.print("failure");
            }
        });


    }

    private final FilmAdapter.OnDeviceSelectedListener filmSelectedListener= new FilmAdapter.OnDeviceSelectedListener(){
        @Override
        public void handle(final Films films) {
            Toast.makeText(FilmActivity.this,"title:  "+films.title+" "+";producer:  "+films.producer+" "+";OpeningCrawl:  "+films.openingCrawl+" "+";edited:  "+films.edited+" "+";director:  "+films.director+" "+";Created:  "+films.created,Toast.LENGTH_SHORT).show();

        }
    };



}
