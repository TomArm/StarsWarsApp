package com.eseo.armange_thomas.eseoapp.main.main;

        import android.app.Application;
        import android.content.Context;
        import android.content.Intent;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.ListView;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.eseo.armange_thomas.eseoapp.R;
        import com.google.gson.Gson;

        import java.util.ArrayList;
        import java.util.List;

        import data.People;
        import data.Planets;
        import data.SWModelList;
        import devices.PeopleAdapter;
        import devices.PlanetsAdapter;
        import retrofit.Callback;
        import retrofit.RetrofitError;
        import retrofit.client.Response;
        import sw.StarWars;
        import sw.StarWarsApi;

/**
 * Created by ARMANGE-Thomas on 16/01/2018.
 */

public class PlanetsActivity extends AppCompatActivity {

    PlanetsAdapter planetsAdapter;
    public static Intent getStartIntent(final Context ctx) {
        return new Intent(ctx, PlanetsActivity.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planets);

        final ArrayList<Planets> planetsArrayList=new ArrayList<Planets>();
        //final ArrayAdapter<Planets> adapter=new ArrayAdapter<Planets>(this,R.layout.list_planets,R.id.textPlanets,planetsArrayList);
        final ArrayList<String> planetsName=new ArrayList<String>();
        final ArrayList<String> planetsContenu=new ArrayList<String>();
        //final ArrayAdapter<String> adapter1=new ArrayAdapter<String>(this, R.layout.list_planets, R.id.textPlanets, planetsName);
        planetsAdapter = new PlanetsAdapter(this,planetsArrayList,planetsSelectedListener);


        ListView listv = (ListView) findViewById(R.id.listPlanets);
        //listv.setAdapter(adapter1);
        listv.setAdapter(planetsAdapter);
        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();
        api.getAllPlanets(2, new Callback<SWModelList<Planets>>() {
            @Override
            public void success(SWModelList<Planets> planetsSWModelList, Response response) {
                planetsArrayList.addAll(planetsSWModelList.results);
                planetsAdapter.notifyDataSetChanged();
            }
            @Override
            public void failure(RetrofitError error) {
                System.out.print("failure");
            }
        });



    }

    /**
     * Triggered when a device is selected
     */
    private final PlanetsAdapter.OnDeviceSelectedListener planetsSelectedListener= new PlanetsAdapter.OnDeviceSelectedListener() {
        @Override
        public void handle(final Planets planets) {
            Toast.makeText(PlanetsActivity.this,"name:  " +planets.name +" "+ ";climate:  "  + planets.climate +" "+ ";terrain:  "+planets.terrain +" "+ ";population:  " + planets.population +" "+ ";gravity:  "+planets.gravity+" "+";surfaceWater  "+planets.surfaceWater,Toast.LENGTH_SHORT).show();
        }
    };






}


