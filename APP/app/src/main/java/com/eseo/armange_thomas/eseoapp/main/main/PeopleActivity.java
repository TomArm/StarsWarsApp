package com.eseo.armange_thomas.eseoapp.main.main;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.eseo.armange_thomas.eseoapp.R;

import java.util.ArrayList;

import data.People;
import data.Planets;
import data.SWModelList;
import devices.PeopleAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sw.StarWars;
import sw.StarWarsApi;

/**
 * Created by ARMANGE-Thomas on 16/01/2018.
 */

public class PeopleActivity extends AppCompatActivity {

    PeopleAdapter peopleAdapter;

    public static Intent getStartIntent(final Context ctx) {
        return new Intent(ctx, PeopleActivity.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people);

        final ArrayList<People> peopleArrayList=new ArrayList<People>();
        //final ArrayAdapter<People> adapter=new ArrayAdapter<People>(this,R.layout.list_people,R.id.textPeople,peopleArrayList);
        final ArrayList<String> peopleContenu=new ArrayList<String>();
        //final ArrayAdapter<String> adapter1=new ArrayAdapter<String>(this, R.layout.list_people, R.id.textPeople,peopleName);


        peopleAdapter = new PeopleAdapter(this,peopleArrayList,peopleSelectedListener);

        ListView listv=(ListView)findViewById(R.id.listPeople);
       // listv.setAdapter(adapter1);
        listv.setAdapter(peopleAdapter);

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();
        api.getAllPeople(2, new Callback<SWModelList<People>>() {
            @Override
            public void success(SWModelList<People> peopleSWModelList, Response response) {
                peopleArrayList.addAll(peopleSWModelList.results);
                peopleAdapter.notifyDataSetChanged();


            }
            @Override
            public void failure(RetrofitError error) {
                System.out.print("failure");
            }
        });


    }
    /**
     * Triggered when a device is selected
     */
    private final PeopleAdapter.OnDeviceSelectedListener peopleSelectedListener = new PeopleAdapter.OnDeviceSelectedListener() {
        @Override
        public void handle(final People people) {
            Toast.makeText(PeopleActivity.this,"Name:  "+ people.name+" "+";BirthYear:  "+ people.birthYear+" "+ ";Created:   " + people.created +" "+ ";Edited:   " + people.edited +" "+ ";Gender:   " +people.gender +" "+ ";HairColor:   " + people.hairColor +" "+ ";Height:   " + people.height +" "+";HomeWorldUrl:   " +people.homeWorldUrl+" "+";Mass:   "+people.mass+" "+ ";SkinColor:   "+ people.skinColor+" "+";Url:   " +people.url,Toast.LENGTH_SHORT).show();

        }
    };

}
