package com.eseo.armange_thomas.eseoapp.main.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;

import com.eseo.armange_thomas.eseoapp.R;

/**
 * Created by ARMANGE-Thomas on 16/01/2018.
 */

public class MenuActivity extends AppCompatActivity {

    private ImageView film;
    private ImageView cars;
    private ImageView people;
    private ImageView planets;
    public static Intent getStartIntent(final Context ctx) {
        return new Intent(ctx, MenuActivity.class);
    }
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        film = findViewById(R.id.iconsFilm);
        cars = findViewById(R.id.iconsCars);
        people = findViewById(R.id.iconsPeople);
        planets = findViewById(R.id.iconsPlanets);

        film.setOnClickListener(onFilmClicked);
        cars.setOnClickListener(onCarsClicked);
        people.setOnClickListener(onPeopleClicked);
        planets.setOnClickListener(onPlanetsClicked);
    }
    private final View.OnClickListener onFilmClicked = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            startActivity(FilmActivity.getStartIntent(MenuActivity.this));
        }
    };

    private final View.OnClickListener onCarsClicked = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            startActivity(CarsActivity.getStartIntent(MenuActivity.this));
        }
    };

    private final View.OnClickListener onPeopleClicked = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            startActivity(PeopleActivity.getStartIntent(MenuActivity.this));
        }
    };

    private final View.OnClickListener onPlanetsClicked = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            startActivity(PlanetsActivity.getStartIntent(MenuActivity.this));
        }
    };
}
