package com.eseo.armange_thomas.eseoapp.main.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.eseo.armange_thomas.eseoapp.R;

import java.util.ArrayList;

import data.Films;
import data.People;
import data.SWModelList;
import data.Vehicle;
import devices.CarsAdapter;
import devices.FilmAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sw.StarWars;
import sw.StarWarsApi;

/**
 * Created by ARMANGE-Thomas on 16/01/2018.
 */

public class CarsActivity extends AppCompatActivity {
    CarsAdapter carsAdapter;
    public static Intent getStartIntent(final Context ctx) {
        return new Intent(ctx, CarsActivity.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars);

        final ArrayList<Vehicle> carsArrayList=new ArrayList<Vehicle>();
        //final ArrayAdapter<Vehicle> adapter=new ArrayAdapter<Vehicle>(this,R.layout.list_cars,R.id.textCars,carsArrayList);
        final ArrayList<String> carsName=new ArrayList<String>();
        final ArrayList<String> carsContenu=new ArrayList<String>();
        //final ArrayAdapter<String> adapter1=new ArrayAdapter<String>(this, R.layout.list_cars, R.id.textCars,carsName);
        carsAdapter = new CarsAdapter(this,carsArrayList,carsSelectedListener);

        ListView listv=(ListView)findViewById(R.id.listCars);
        //listv.setAdapter(adapter1);
        listv.setAdapter(carsAdapter);
        //listv.setOnItemClickListener(new CarsActivity.ItemList());

        StarWarsApi.init();
        StarWars api = StarWarsApi.getApi();
        api.getAllVehicles(2, new Callback<SWModelList<Vehicle>>() {
            @Override
            public void success(SWModelList<Vehicle> vehicleSWModelList, Response response) {
                carsArrayList.addAll(vehicleSWModelList.results);
                carsAdapter.notifyDataSetChanged();
            }
            @Override
            public void failure(RetrofitError error) {
                System.out.print("failure");
            }
        });


    }
    private final CarsAdapter.OnDeviceSelectedListener carsSelectedListener= new CarsAdapter.OnDeviceSelectedListener(){
        @Override
        public void handle(final Vehicle vehicle) {
            Toast.makeText(CarsActivity.this,"Name:  " +vehicle.name+" "+";VehiculeClass:  "+vehicle.vehicleClass+" "+";Passengers:  "+vehicle.passengers+" "+";Model:  "+vehicle.model+" "+";Length:  "+vehicle.length+" "+";CargoCapacity:  "+vehicle.cargoCapacity,Toast.LENGTH_SHORT).show();

        }
    };

}
